# Lab 1 - Create a Camel Route and Deploy to OpenShift

## Open CodeReady Workspaces 

Click on CodeReady Workspaces on the right-hand side under Applications

![lab1-select-codeready.png](./images/lab1-select-codeready.png)

## Login 

Using your userid (evals<uid>) and add your first and last name when prompted and click submit.

![lab1-codeready-login.png](./images/lab1-add-name-login-codeready.png)

## Create a Workspace

After clicking submit you should land on a page similar to the following where you will create your workspace to begin writing code. 

![lab1-codeready-landing.png](./images/lab1-codeready-landing.png)

Name your workspace something unique to you. 

![lab1-name.png](./images/lab1-name.png)

Then select the Fuse stack. 

![lab1-select-fuse-stack.png](./images/lab1-select-fuse-stack.png)

Next, click on 'Add or Import Project'

![lab1-add-project.png](./images/lab1-add-project.png)

Select Git and put the following in the Git URL field: "https://gitlab.com/redhatsummitlabs/learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift.git"

![lab1-giturl.png](./images/lab1-giturl.png)

Select Add 
	
![lab1-create-workspace.png](./images/lab1-create-workspace.png)

Select Create and Open

![lab1-create-open.png](./images/lab1-create-open.png)

## Create Your Camel Route

After creating your workspace it may take a few moments to load.  After loading you should see a screen like this. 

![lab1-workspace-landing.png](./images/lab1-workspace-landing.png)

Expand your folders on the left hand side so you can see the SampleCamelRouter under rest-dsl/src/main/java/com/sample/camel/route

![lab1-folders.png](./images/lab1-folders.png)

Open up the SampleCamelApplication under rest-dsl/src/main/java/com/sample/camel.  Notice the TODO. 

![lab1-sample-camel-app.png](./images/lab1-sample-camel-app.png)

Take note of the class and add the `ServletRegistrationBean`.  This is necessary to use the Camel Servlet component to write your APIs using the Camel RestDSL.

TIP: In Fuse 7 onwards this is no longer necessary.

```java
    @Bean
    public ServletRegistrationBean camelServletRegistrationBean() {
        ServletRegistrationBean registration = new ServletRegistrationBean(new CamelHttpTransportServlet(), "/camel/*");
        registration.setName("CamelServlet");
        return registration;
    }
```

Notice your files will automatically save. 

Next, open your SampleCamelRouter under rest-dsl/src/main/java/com/sample/camel/router.  Notice the TODOs. 

![lab1-sample-camel-router.png](./images/lab1-sample-camel-router.png)

First we need to add the rest configuration.  In this case we are using the camel-servlet component and binding to JSON. 

```java
        restConfiguration()
        	.component("servlet")
    		.bindingMode(RestBindingMode.json);
```

Next, we need to write the actual camel route.  We start by defining the REST endpoint at a get endpoint, give it a routeid, and then direct it to the second part of the route with an asynchronous "direct" call. 

```java
        rest()
            .get("/hello").produces("text/plain")
            .responseMessage().code(200).message("OK").endResponseMessage()
            .route().routeId("say-hello")
            .to("direct:hello");
        
        from("direct:hello")
        	.routeId("log-hello")
        	.log(LoggingLevel.INFO, "Hello World")
        	.transform().simple("Hello World");
```

Now that our camel route is written, we just need to add the proper plugin to be able to deploy it to openshift. Open up the pom.xml file under rest-dsl and scroll to around like 134

![lab1-pom.png](./images/lab1-pom.png)

Add the fabric8-maven-plugin.  This will allow up to deploy our camel-route to openshift. 

```xml
      <plugin>
        <groupId>io.fabric8</groupId>
        <artifactId>fabric8-maven-plugin</artifactId>
        <version>${fabric8.maven.plugin.version}</version>
        <executions>
          <execution>
            <goals>
              <goal>resource</goal>
              <goal>build</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
```

## Deploy to OpenShift

In the bottom right double click on terminal 

![lab1-terminal.png](./images/lab1-terminal.png)

In the terminal type the following command and when prompted enter your credentials (evals#/Password1)

```
$ oc login https://master.07c0.summit.opentlc.com
```

![lab1-terminal-login.png](./images/lab1-terminal-login.png)

You are now successfully logged into your OpenShift cluster and are able to deploy projects! First create a new project with a name unique to you. After running this command you should automatically be set up to deploy to this project. 

```
$ oc new-project <yourname>-camel
```

![lab1-new-project.png](./images/lab1-new-project.png)

Now, you need to navigate to the project home directory for your camel route. In this case learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/rest-dsl.  Then to deploy to openshift just run the mvn fabric8 command seen below. 

```
$ cd learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/rest-dsl
$ mvn clean fabric8:deploy 
```

The command will download some dependencies and may take a few minutes. If all goes well you should see something like below. 

![lab1-deployment-success.png](./images/lab1-deployment-success.png)

## Test Out Your Camel Route

Navigate to https://master.07c0.summit.opentlc.com and login

On the right hand side select your project

![lab1-select-project.png](./images/lab1-select-project.png)

You will land on a page similar to this and see your camel deployment

![lab1-project-landing.png](./images/lab1-project-landing.png)

Click on your cooresponding link to the one circled in red 

![lab1-camel-link.png](./images/lab1-camel-link.png)

As you will see nothing is there.  Go ahead and append /camel/hello to it to hit your active rest endpoint

![lab1-hit-url.png](./images/lab1-hit-url.png)

Congrats! This completes lab 1.  You can move on to Lab 2. 

[Go To Lab 2](https://gitlab.com/redhatsummitlabs/learning-to-use-the-camel-rest-dsl-with-3scale-and-openshift/blob/master/lab2.md)